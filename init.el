;; init.el --- Emacs configuration

;; --------------------------------------
;; INSTALL PACKAGES
;; --------------------------------------

;; load emacs 24's package system. Add MELPA repository.
(require 'package)
(add-to-list 'package-archives
             ;; '("melpa" . "http://stable.melpa.org/packages/") ;; many packages won't show if using stable
             '("melpa" . "http://melpa.milkbox.net/packages/"))

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))

(defvar myPackages
  '(smex
    ein
    elpy
    flycheck
    py-autopep8
    monokai-theme))

(mapc #'(lambda (package)
    (unless (package-installed-p package)
      (package-install package)))
      myPackages)


;; --------------------------------------
;; BASIC CUSTOMIZATION
;; --------------------------------------

;; (setq inhibit-startup-message t) ;; hide the startup message
(load-theme 'monokai t)             ;; load monokai theme
;; (global-linum-mode t)            ;; enable line numbers globally
(tool-bar-mode -1)                  ;; disable the toolbar
;; (menu-bar-mode -1)               ;; disable the menu bar

;; Smex (M-x enhancement for Emacs)
(require 'smex)    ;; Not needed if you use package.el
(smex-initialize)  ;; Can be omitted. This might cause a (minimal) delay
                   ;; when Smex is auto-initialized on its first run.
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)  ;; This is your old M-x.


;; --------------------------------------
;; PYTHON CONFIGURATION
;; --------------------------------------

(elpy-enable)
(elpy-use-ipython)

;; use flycheck not flymake with elpy
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; enable autopep8 formatting on save
(require 'py-autopep8)
(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)


;; init.el ends here
